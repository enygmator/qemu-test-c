#include <stdio.h>

#if defined(__redox__)
int sum(int a, int b)
{
    return a + b;
}
#endif

int main(void)
{
    int a = 0;
    int b = 0;

    printf("Enter two numbers: ");
    scanf("%d %d", &a, &b);

    printf("%d + %d = %d\n", a, b, sum(a, b));
}
