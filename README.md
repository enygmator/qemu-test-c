# qemu-test-c

author: tarun.aditya@hotmail.com  
mattermost chat username: enygmator

This repo has nothing to do with QEMU, and is meant only to demonstrate compilation with `x86_64-unknown-redox-gcc`

## Compilation and Execution

> This repo is meant to be compiled from and run on Redox OS, and while it can be run on other platforms, the makefile may need tweaking.


1. Assume you have Redox OS in the path `$TopLevel/redox`, where `$TopLevel` can be any path.
2. Place this repo in `$TopLevel/redox_apps/`, such that the path of this folder is `$TopLevel/redox_apps/qemu-test-c`
3. Go to `$TopLevel/redox` in the terminal and run the command `make cook PKG=qemu-test-c`.
   NOTE: Before running the command, it would be better if you have compiled and run Redox OS at least once (since, `prefix` is required to have been created correctly for this command to work).
4. If the command runs successfully, then you have to include the "cooked" package, in the final `harddrive.bin` that emulated the harddrive of Redox OS, when run in QEMU. To do that, you'll simply have to include the line `qemu-test-c = {}` in `filesystem.toml`, under the table `[packages]`.
   `filesystem.toml` points to one of the files in the `config/` folder.
5. Now, it's time to compile and generate `harddrive.bin` with Redox OS and qemu-test-c installed on it, and load it into QEMU. From `$TopLevel/redox`, execute:
   ```
   touch filesystem.toml
   make qemu
   ```
6. Inside `Redox OS`, you can open the terminal and run the command `qemu-test-c`.

The "recipe" to "cook" this program/`package` can be found in the Redox OS "cookbook", under the folder `qemu-test-c` (`$TopLevel/redox/cookbook/recipes/qemu-test-c/`).

The recipe must provide the correct `CFLAGS`, like `-g`, if compiling for debugging with a debugger, and `-static` (since Redox OS only works with static at the moment of writing).

> This can also be installed through `Redoxer`, which is a toll installed via `cargo install redoxer`, that automatically takes an app and deploys it on an automatically cloned and compiled version of Redox OS. But it hasn't been tried.



