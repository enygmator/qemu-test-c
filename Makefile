.PHONY: all install

DESTDIR = "$(realpath $(DESTDIR))" 

all: qemu-test-c

install: qemu-test-c
	@echo "Installing..."
	@mkdir -p $(DESTDIR)/bin
	cp -r ./bin/* $(DESTDIR)/bin/

qemu-test-c: fun.c | ./bin
	@echo "Compiling..."
	${CC} -o ./bin/$@ $< ${CFLAGS}

./bin:
	@echo "Creating bin directory..."
	mkdir -p ./bin